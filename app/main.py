import uvicorn

from fastapi import APIRouter, FastAPI

from api.routers.salaries import salary_router
from api.routers.users import user_router

app = FastAPI(title="Salaries")

router = APIRouter()

router.include_router(user_router, prefix="/users", tags=["users"])
router.include_router(salary_router, prefix="/salary", tags=["salaries"])

app.include_router(router)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
