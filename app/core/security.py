from jose import jwt
from passlib.context import CryptContext

from core.config import ALGORITHM, SECRET_KEY

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Hasher:
    @staticmethod
    def verify_password(password: str, hash_password: str) -> bool:
        return pwd_context.verify(password, hash_password)

    @staticmethod
    def hash_password(password: str) -> str:
        return pwd_context.hash(password)


async def create_access_token(data: dict) -> str:
    """Создание токена"""

    encoded_jwt = jwt.encode(claims=data, key=SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt
